FROM python:3.5 as intermediate

# create and change working directory

WORKDIR /code

# Add credentials on build

ARG SSH_PRIVATE_KEY

RUN mkdir /root/.ssh/

# remember to use a temporary variable for this

# This private key shouldn't be saved in env files

RUN echo "${SSH_PRIVATE_KEY}" >> /root/.ssh/id_rsa && chmod 600 /root/.ssh/id_rsa

# make sure your domain is accepted

RUN touch /root/.ssh/known_hosts

RUN ssh-keyscan gitlab.com >> /root/.ssh/known_hosts
RUN git clone git@gitlab.com:radicallyopensecurity/roscore.git /code

# image that would be used

# FROM rocketchat/hubot-rocketchat:latest
FROM registry.gitlab.com/radicallyopensecurity/hubot-pentext:latest

MAINTAINER Radically Open Security <infra@radicallyopensecurity.com>

USER root

COPY --from=intermediate /code /code

RUN apk add --no-cache libffi-dev \
    && apk add --no-cache --virtual .build-deps mariadb-dev \
    && pip3 install --no-cache-dir -e /code \
    && apk add --virtual .runtime-deps mariadb-client-libs \
    && apk del .build-deps

USER hubot

WORKDIR /home/hubot
