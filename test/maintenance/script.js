'use strict'

const maintenance = require('../../scripts/maintenance.js')

module.exports = (robot) => {
  robot.respond('/deprecated$/i', {
    id: 'deprecated'
  }, maintenance.deprecate)

  robot.respond('/deprecated-with-custom-message$/i', {
    id: 'deprecated'
  }, (res) => maintenance.deprecate(res, 'A deprecated command with a custom message.'))

  robot.respond('/wip/i', {
    id: 'wip'
  }, (res) => maintenance.wip(res, 'https://example.com'))
}
