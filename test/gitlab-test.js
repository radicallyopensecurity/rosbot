'use strict'

/* global describe, it */

const assert = require('assert')
const gitlab = require('../scripts/gitlab.js')

describe('gitlab', () =>
  it('parses web URL from gitlab output', function () {
    const input = `\
id: 12
assignee: None
author:
  id: 1
  name: Test User
  state: active
  username: test
  web-url: https://gitlabs/test
confidential: False
created-at: 2018-05-02T07:38:54.888Z
description: None
downvotes: 0
due-date: None
iid: 7
labels: [ feature ]
milestone: None
project-id: 761
state: opened
subscribed: True
title: test title
updated-at: 2018-05-02T07:38:54.888Z
upvotes: 0
user-notes-count: 0
web-url: https://gitlabs/some-namespace/repo/issues/7\
`
    const url = gitlab.parse_web_url_from_gitlab_output(input)
    assert.strictEqual(url, 'https://gitlabs/some-namespace/repo/issues/7')
  })
)
